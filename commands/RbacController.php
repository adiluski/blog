<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
	public function actionInitp()
	{
		$auth = Yii::$app->authManager;

		// add "allWithUsers" permission *
		$allWithUsers = $auth->createPermission('allWithUsers');
		$allWithUsers->description = 'Do everything with users';
		$auth->add($allWithUsers);
		
		// add "assignUsersToPost" permission *
		$assignUsersToPost = $auth->createPermission('assignUsersToPost');
		$assignUsersToPost->description = 'Assign or reassign users to posts';
		$auth->add($assignUsersToPost);

		// add "addPosts" permission * 
		$addPosts = $auth->createPermission('addPosts');
		$addPosts->description = 'Add new Posts';
		$auth->add($addPosts);

		// add "addOwnPost" permission * 
		$addOwnPost = $auth->createPermission('addOwnPost');
		$addOwnPost->description = 'Add own post';
		$auth->add($addOwnPost);
		
		// add "updatePosts" permission *
		$updatePosts = $auth->createPermission('updatePosts');
		$updatePosts->description = 'update new Posts';
		$auth->add($updatePosts);

		// add "updateOwnPost" permission *
		$updateOwnPost = $auth->createPermission('updateOwnPost');
		$updateOwnPost->description = 'update own post';
		$auth->add($updateOwnPost);	
		
		// add "viewPosts" permission * 
		$viewPosts = $auth->createPermission('viewPosts');
		$viewPosts->description = 'view Posts';
		$auth->add($viewPosts);

		// add "viewOwnPost" permission *
		$viewOwnPost = $auth->createPermission('viewOwnPost');
		$viewOwnPost->description = 'view own post';
		$auth->add($viewOwnPost);

		// add "indexPosts" permission *
		$indexPosts = $auth->createPermission('indexPosts');
		$indexPosts->description = 'index Posts';
		$auth->add($indexPosts);

		// add "indexOwnPost" permission *
		$indexOwnPost = $auth->createPermission('indexOwnPost');
		$indexOwnPost->description = 'index own post';
		$auth->add($indexOwnPost);			

		
		// add "deletePosts" permission *
		$deletePosts = $auth->createPermission('deletePosts');
		$deletePosts->description = 'delete Posts';
		$auth->add($deletePosts);
		
		// add "changeStatusToCancel" permission * 
		$changeStatusToCancel = $auth->createPermission('changeStatusToCancel');
		$changeStatusToCancel->description = 'Change post status to cancel';
		$auth->add($changeStatusToCancel);		
	
	}
		
	public function actionInitr()
	{
		$auth = Yii::$app->authManager;				
		
		// add "Author" role 
		$author = $auth->createRole('author');
		$auth->add($author);
		
		// add "admin" role 
		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}
	public function actionInitc()
	{
		$auth = Yii::$app->authManager;				
		

		//add childes to Admin  
		$admin = $auth->getRole('admin');
		$AllWithUsers = $auth->getPermission('allWithUsers');
		$auth->addChild($admin, $AllWithUsers);
		
		$admin = $auth->getRole('admin');
		$assignUsersToPost = $auth->getPermission('assignUsersToPost');
		$auth->addChild($admin, $assignUsersToPost);		

		$admin = $auth->getRole('admin');
		$updatePosts = $auth->getPermission('updatePosts');
		$auth->addChild($admin, $updatePosts);

		$admin = $auth->getRole('admin');
		$viewPosts = $auth->getPermission('viewPosts');
		$auth->addChild($admin, $viewPosts);	

		$admin = $auth->getRole('admin');
		$indexPosts = $auth->getPermission('indexPosts');
		$auth->addChild($admin, $indexPosts);

		$admin = $auth->getRole('admin');
		$deletePosts = $auth->getPermission('deletePosts');
		$auth->addChild($admin, $deletePosts);
		
		$admin = $auth->getRole('admin');
		$changeStatusToCancel = $auth->getPermission('changeStatusToCancel');
		$auth->addChild($admin, $changeStatusToCancel);		

		// **
		$author = $auth->getRole('author');
		$addPosts = $auth->getPermission('addPosts');
		$auth->addChild($author, $addPosts);	

		//**
		$author = $auth->getRole('author');
		$updateOwnPost = $auth->getPermission('updateOwnPost');
		$auth->addChild($author, $updateOwnPost);	

		//**
		$author = $auth->getRole('author');
		$viewOwnPost = $auth->getPermission('viewOwnPost');
		$auth->addChild($author, $viewOwnPost);

		//**
		$author = $auth->getRole('author');
		$indexOwnPost = $auth->getPermission('indexOwnPost');
		$auth->addChild($author, $indexOwnPost);	


		$addOwnPost = $auth->getPermission('addOwnPost');
		$addPosts = $auth->getPermission('addPosts');
		$auth->addChild($addOwnPost, $addPosts);	

		$updateOwnPost = $auth->getPermission('updateOwnPost');
		$updatePosts = $auth->getPermission('updatePosts');
		$auth->addChild($updateOwnPost, $updatePosts);

		$viewOwnPost = $auth->getPermission('viewOwnPost');
		$viewPosts = $auth->getPermission('viewPosts');
		$auth->addChild($viewOwnPost, $viewPosts);

		$indexOwnPost = $auth->getPermission('indexOwnPost');
		$indexPosts = $auth->getPermission('indexPosts');
		$auth->addChild($indexOwnPost, $indexPosts);

		$admin = $auth->getRole('admin');		
		$author = $auth->getRole('author');
		$auth->addChild($admin, $author);
	}	
	
	public function actionInitrule()
	{
		
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnPostRule;
		$auth->add($rule);
	}

	public function actionAddrule()
	{

		$auth = Yii::$app->authManager;	
		$rule = $auth->getRule('isOwner');
		$addOwnPost = $auth->getPermission('addOwnPost');
		$addOwnPost->ruleName = $rule->name;
		$auth->add($addOwnPost);
		$updateOwnPost = $auth->getPermission('updateOwnPost');
		$updateOwnPost->ruleName = $rule->name;	
		$auth->add($updateOwnPost);		
		$viewOwnPost = $auth->getPermission('viewOwnPost');
		$viewOwnPost->ruleName = $rule->name;
		$auth->add($viewOwnPost);
		
	}

}
