<?php

namespace app\controllers;

use Yii;
use app\models\Post;
use app\models\User;
use app\models\Status;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UnauthorizedHttpException;
 

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['delete','create','index'],
				'rules' => [
					[
						'actions' => ['delete',],
						'allow' => true,
						'roles' => ['deletePosts'],
					],
					[
						'actions' => ['create',],
						'allow' => true,
						'roles' => ['addPosts'],
					],
					[
						'actions' => ['index',],
						'allow' => true,
						'roles' => ['indexOwnPost'],
					],						
				],
			],		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$status = $searchModel->statusId;
		if (!$status) $status = -1;
		$user = $searchModel->AuthorId;
		if (!$user) $user = -1;
		$statuses = Status::getStatuses();
		$users = User::getUsers();
		$users[-1] = "All";
		$statuses[-1] = "All";
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'status' => $status,
			'user' => $user, 
			'statuses' => $statuses,
			'users' => $users
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    $post = $this->findModel($id);
	if (!\Yii::$app->user->can('viewPosts', ['post' => $post]))
			throw new UnauthorizedHttpException 
			('Hey, this is not your Post!');

	return $this->render('view', [
            'model' => $post,
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();
		$users = User::getUsers();
		$statuses = Status::getStatuses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'users' => $users,
				'statuses' => $statuses,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		 $post = $this->findModel($id);
		
		if (!\Yii::$app->user->can('updatePosts', ['post' => $post]))
			throw new UnauthorizedHttpException 
			('Hey, this is not your Post!');
			
		$users = User::getUsers();
		$statuses = Status::getStatuses();	

        if ($post->load(Yii::$app->request->post()) && $post->save()) {
            return $this->redirect(['view', 'id' => $post->id]);
        } else {
            return $this->render('update', [
                'model' => $post,
				'users' => $users,
				'statuses' => $statuses,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
