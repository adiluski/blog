<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;

/**
 * PostSearch represents the model behind the search form about `app\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'AuthorId', 'statusId'], 'integer'],
            [['title', 'body'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       if ($this->statusId == -1) $this->statusId = '';
		if ($this->AuthorId == -1) $this->AuthorId = '';
		$query->andFilterWhere([
            'id' => $this->id,
            'statusId' => $this->statusId,
        ]);
		if (!\Yii::$app->user->can('indexPosts'))
		{
			$query->andFilterWhere([
				'AuthorId' => Yii::$app->user->id,
			]);
		}
		else
		{
			$query->andFilterWhere([
				'AuthorId' => $this->AuthorId,
			]);			
		}		

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }
}
