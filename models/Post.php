<?php

namespace app\models;

use Yii;
use yii\filters\AccessControl;
use yii\web\UnauthorizedHttpException;
 

/**
 * This is the model class for table "Post".
 *
 * @property integer $id
 * @property integer $AuthorId
 * @property integer $statusId
 * @property string $title
 * @property string $body
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AuthorId', 'statusId'], 'integer'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'AuthorId' => 'Author ID',
            'statusId' => 'Status ID',
            'title' => 'Title',
            'body' => 'Body',
        ];
    }
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);
		
		if (!\Yii::$app->user->can('assignUsersToPost'))
			$this->AuthorId = Yii::$app->user->id;
		if (!\Yii::$app->user->can('changeStatusToCancel') AND 
				$this->statusId == 3 )
		throw new UnauthorizedHttpException 
			('You are not allowed to cancel Posts!');			
        return $return;
    }
	public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'AuthorId']);
    }
    public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
     }		
}
