<?php

use yii\db\Schema;
use yii\db\Migration;

class m150621_132738_init_status_table extends Migration
{
   public function up()
    {
		$this->createTable(
		'status',
			[
				'id' => 'pk',
				'name' => 'string',
			]
		);
    }

    public function down()
    {
		$this->dropTable('status');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
