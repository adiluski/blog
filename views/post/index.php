<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           [
                'attribute' => 'AuthorId',
                'label' => 'The Author',
				'filter'=>Html::dropDownList('PostSearch[AuthorId]', $user, $users, ['class'=>'form-control']),
                'format' => 'html',
				'value' => function ($data) {
						return Html::a($data->author->username,
							['user/view', 'id' => $data->author->id]);
                    }			
            ],
			[
                'attribute' => 'statusId',
                'label' => 'Status',
				'filter'=>Html::dropDownList('PostSearch[statusId]', $status, $statuses, ['class'=>'form-control']),
                'value' => function ($data) {
						$statusName = $data->statusItem->name; 
                        return $statusName;
                    }
            ],			
            'title',
            'body:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
